| Book Name | Remarks |Status| Store Link |
|:----------|:-------:|:-----|:-----------|
| Eloquent Ruby | To know ruby way of thinking and in turn write beautiful code | Yet to be purchased | [amazon store](http://www.amazon.in/Eloquent-Ruby-Addison-Wesley-Professional-Series/dp/0321584104) |
| The Art of Thinking Clearly | To write good code, first you have to think clearly | Purchased | [amazon store](http://www.amazon.in/Art-Thinking-Clearly-Rolf-Dobelli/dp/144475954X/) |
| POODR | learn how to write object-oriented code | Yet to be purchased | [amazon store](http://www.amazon.in/Practical-Object-Oriented-Design-Ruby-Addison-Wesley/dp/0321721330/) |
| Rich Dad Poor Dad | Finance Book | Yet to be purchased | [amazon store](http://www.amazon.in/Rich-Dad-Poor-Teach-Middle/dp/1612680011) |
